let previousContent = '';
document.getElementById('markdown-input').style.display = 'none';
document.getElementById('input-container').style.display = 'none';
document.getElementById('explanation1').style.display = 'none';
document.getElementById('buttonToAddNewFile').style.display = 'none';
document.getElementById('buttonToaddNewAuthor').style.display = 'none';
document.getElementById('buttonForNewFindingsSummary').style.display = 'none';
document.getElementById('buttonToAddNewRolesAndActors').style.display = 'none';
document.getElementById('replacePlaceholders').style.display = 'none';

// Function to update the preview
function updatePreview() {
    const markdownInput = document.getElementById('markdown-input').value;
    const converter = new showdown.Converter();
    const html = converter.makeHtml(markdownInput);

    // Update the preview only if the content has changed
    if (markdownInput !== previousContent) {
        document.getElementById('markdown-container').innerHTML = html;
        previousContent = markdownInput;
    }
}

// Function to load template content into textarea
function loadTemplate(event) {
    const fileInput = event.target;
    const templateFile = fileInput.files[0];

    if (templateFile) {
        const reader = new FileReader();
        reader.onload = function (e) {
            const templateContent = e.target.result;
            document.getElementById('markdown-input').value = templateContent;
            document.getElementById('markdown-input').style.display = 'block';
            document.getElementById('buttonForNewFindingsSummary').style.display = 'block';
            document.getElementById('input-container').style.display = 'block';
            document.getElementById('buttonToaddNewAuthor').style.display = 'block';
            document.getElementById('buttonToAddNewRolesAndActors').style.display = 'block';
            document.getElementById('buttonToAddNewFile').style.display = 'block';
            document.getElementById('replacePlaceholders').style.display = 'block';
            document.getElementById('explanation').style.display = 'none';
            document.getElementById('explanation1').style.display = 'block';
            document.getElementById('download-button').style.display = 'block';
            updatePreview();
            // Hide the file input
            fileInput.style.display = 'none';
        };

        reader.readAsText(templateFile);
    }
}

// Watch for changes in the textarea and update the preview
document.getElementById('markdown-input').addEventListener('input', updatePreview);

// Periodically check for changes
setInterval(updatePreview, 1000);

// Function to download the modified Markdown content
function downloadMarkdown() {
    const markdownContent = document.getElementById('markdown-input').value;
    const blob = new Blob([markdownContent], { type: 'text/markdown' });

    const link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.download = 'modified-template.md';
    document.body.appendChild(link);
    link.click();

    // Open CloudConvert link in a new tab
    window.open(`https://cloudconvert.com/md-to-pdf`, '_blank');

    document.body.removeChild(link);
}

let roleAndActorCounter = 0;

let authorCounter = 0;

let fileCounter = 0;

let findingsCounter = 0;

async function replacePlaceholders() {

    // Get values from HTML inputs
    const introduction = document.getElementById('TEXT-FOR-INTRODUCTION').value;
    const auditedBy = document.getElementById('TEXT-FOR-AUDITED-BY').value;
    const disclaimer = document.getElementById('TEXT-FOR-DISCLAIMER').value;
    const aboutMe = document.getElementById('TEXT-FOR-ABOUT-ME').value;
    const privilegedRolesOwner = document.getElementById('TEXT-FOR-ROLES-AND-ACTORS').value;
    const privilegedRolesDescriptionOwner = document.getElementById('DESCRIPTION-FOR-ROLES-AND-ACTORS').value;
    const protocolSummaryProjectName = document.getElementById('PROJECT-NAME').value;
    const protocolSummaryRepository = document.getElementById('REPOSITORY').value;
    const protocolSummaryAuditTimeline = document.getElementById('AIDIT-TIMELINE').value;
    const protocolSummaryReviewCommitHash = document.getElementById('REVIEW-COMMIT-HASH').value;
    const fileForTheScope = document.getElementById('FILE-FOR-THE-SCOPE').value;
    const issuesCritical = document.getElementById('NUMBER-OF-CRITICAL-ISSUES').value;
    const issuesHigh = document.getElementById('NUMBER-OF-HIGH-ISSUES').value;
    const issuesMedium = document.getElementById('NUMBER-OF-MEDIUM-ISSUES').value;
    const issuesLow = document.getElementById('NUMBER-OF-LOW-ISSUES').value;
    const issuesInformational = document.getElementById('NUMBER-OF-INFORMATIONAL-ISSUES').value;
    const summaryId = document.getElementById('DESCRIPTION-ID-FOR-FINDINGS-SUMMARY').value;
    const summaryTitle = document.getElementById('DESCRIPTION-TITLE-FOR-FINDINGS-SUMMARY').value;
    const summarySeverity = document.getElementById('DESCRIPTION-SEVERITY-FOR-FINDINGS-SUMMARY').value;
    const informationForImpactSeverity = document.getElementById('INFORMATION-FOR-IMPACT-SEVERITY').value;
    const informationForLikelihoodSeverity = document.getElementById('INFORMATION-FOR-LIKELIHOOD-SEVERITY').value;
    const textForVulnerabilityDetails = document.getElementById('TEXT-FOR-VULNERABILITY-DETAILS').value;
    const textForRecommendation = document.getElementById('TEXT-FOR-RECOMMENDATION').value;
    const codeType = document.getElementById('CODE-TYPE').value;
    const codeSnippet = document.getElementById('CODE-SNIPET').value;
    const fixedOrNot = document.getElementById('FIXED-OR-NOT').value;

    // Get current Markdown content
    let markdownContent = document.getElementById('markdown-input').value;

    // Replace placeholders in the Markdown content
    markdownContent = markdownContent.replace('!TEXT-FOR-INTRODUCTION!', introduction);
    markdownContent = markdownContent.replace('!TEXT-FOR-AUDITED-BY!', auditedBy);
    markdownContent = markdownContent.replace('!TEXT-FOR-DISCLAIMER!', disclaimer);
    markdownContent = markdownContent.replace('!TEXT-FOR-ABOUT-ME!', aboutMe);
    markdownContent = markdownContent.replace('!TEXT-FOR-ROLES-AND-ACTORS!', privilegedRolesOwner);
    markdownContent = markdownContent.replace('!DESCRIPTION-FOR-ROLES-AND-ACTORS!', privilegedRolesDescriptionOwner);
    markdownContent = markdownContent.replace('!PROJECT-NAME!', protocolSummaryProjectName);
    markdownContent = markdownContent.replace('!REPOSITORY!', protocolSummaryRepository);
    markdownContent = markdownContent.replace('!AIDIT-TIMELINE!', protocolSummaryAuditTimeline);
    markdownContent = markdownContent.replace('!REVIEW-COMMIT-HASH!', protocolSummaryReviewCommitHash);
    markdownContent = markdownContent.replace('!FILE-FOR-THE-SCOPE!', fileForTheScope);
    markdownContent = markdownContent.replace('!NUMBER-OF-CRITICAL-ISSUES!', issuesCritical);
    markdownContent = markdownContent.replace('!NUMBER-OF-HIGH-ISSUES!', issuesHigh);
    markdownContent = markdownContent.replace('!NUMBER-OF-MEDIUM-ISSUES!', issuesMedium);
    markdownContent = markdownContent.replace('!NUMBER-OF-LOW-ISSUES!', issuesLow);
    markdownContent = markdownContent.replace('!NUMBER-OF-INFORMATIONAL-ISSUES!', issuesInformational);
    markdownContent = markdownContent.replaceAll('!DESCRIPTION-ID-FOR-FINDINGS-SUMMARY!', summaryId);
    markdownContent = markdownContent.replaceAll('!DESCRIPTION-TITLE-FOR-FINDINGS-SUMMARY!', summaryTitle);
    markdownContent = markdownContent.replace('!DESCRIPTION-SEVERITY-FOR-FINDINGS-SUMMARY!', summarySeverity);
    markdownContent = markdownContent.replace('!INFORMATION-FOR-IMPACT-SEVERITY!', informationForImpactSeverity);
    markdownContent = markdownContent.replace('!INFORMATION-FOR-LIKELIHOOD-SEVERITY!', informationForLikelihoodSeverity);
    markdownContent = markdownContent.replace('!TEXT-FOR-VULNERABILITY-DETAILS!', textForVulnerabilityDetails);
    markdownContent = markdownContent.replace('!TEXT-FOR-RECOMMENDATION!', textForRecommendation);
    markdownContent = markdownContent.replace('!CODE-TYPE!', codeType);
    markdownContent = markdownContent.replace('!CODE-SNIPET!', codeSnippet);
    markdownContent = markdownContent.replace('!FIXED-OR-NOT!', fixedOrNot);

    markdownContent = await populateRolesAndActors(markdownContent);

    markdownContent = await populateAuthors(markdownContent);

    markdownContent = await populateFiles(markdownContent);

    markdownContent = await populateFindings(markdownContent);

    // Update the Markdown content in the textarea
    document.getElementById('markdown-input').value = markdownContent;

    // Update the preview
    updatePreview();
}

async function populateFindings(markdownContent) {
    for (let i = 0; i < findingsCounter; i++) {
        const descriptionIdForFindingsSummaryIndex = `DESCRIPTION-ID-FOR-FINDINGS-SUMMARY-${i}`;
        const descriptionTitileForFindingsSummaryIndex = `DESCRIPTION-TITLE-FOR-FINDINGS-SUMMARY-${i}`;
        const descriptionSeverityForFindingsSummaryIndex = `DESCRIPTION-SEVERITY-FOR-FINDINGS-SUMMARY-${i}`;
        const informationForImpactSeverityIndex = `INFORMATION-FOR-IMPACT-SEVERITY-${i}`;
        const informationForLikelihoodSeverityIndex = `INFORMATION-FOR-LIKELIHOOD-SEVERITY-${i}`;
        const textForVulnerabilityDetailsIndex = `TEXT-FOR-VULNERABILITY-DETAILS-${i}`;
        const textForRecommendationIndex = `TEXT-FOR-RECOMMENDATION-${i}`;
        const codeTypeIndex = `CODE-TYPE-${i}`;
        const codeSnippetIndex = `CODE-SNIPET-${i}`;
        const fixedOrNotIndex = `FIXED-OR-NOT-${i}`;

        const descriptionIdForFindingsSummaryIndexValue = document.getElementById(descriptionIdForFindingsSummaryIndex).value;
        const descriptionTitileForFindingsSummaryIndexValue = document.getElementById(descriptionTitileForFindingsSummaryIndex).value;
        const descriptionSeverityForFindingsSummaryIndexValue = document.getElementById(descriptionSeverityForFindingsSummaryIndex).value;
        const informationForImpactSeverityIndexValue = document.getElementById(informationForImpactSeverityIndex).value;
        const informationForLikelihoodSeverityValue = document.getElementById(informationForLikelihoodSeverityIndex).value;
        const textForVulnerabilityDetailsValue = document.getElementById(textForVulnerabilityDetailsIndex).value;
        const textForRecommendationValue = document.getElementById(textForRecommendationIndex).value;
        const codeTypeValue = document.getElementById(codeTypeIndex).value;
        const codeSnippetValue = document.getElementById(codeSnippetIndex).value;
        const fixedOrNotValue = document.getElementById(fixedOrNotIndex).value;

        const descriptionIdForFindingsSummaryIndexValueNewIndexes = `${descriptionIdForFindingsSummaryIndexValue}`;
        const descriptionTitileForFindingsSummaryIndexValueNewIndexes = `${descriptionTitileForFindingsSummaryIndexValue}`;
        const descriptionSeverityForFindingsSummaryIndexValueNewIndexes = `${descriptionSeverityForFindingsSummaryIndexValue}`;
        const informationForImpactSeverityIndexValueNewIndexes = `${informationForImpactSeverityIndexValue}`;
        const informationForLikelihoodSeverityValueNewIndexes = `${informationForLikelihoodSeverityValue}`;
        const textForVulnerabilityDetailsValueNewIndexes = `${textForVulnerabilityDetailsValue}`;
        const textForRecommendationValueNewIndexes = `${textForRecommendationValue}`;
        const codeTypeValueIndexes = `${codeTypeValue}`;
        const codeSnippetValueIndexes = `${codeSnippetValue}`;
        const fixedOrNotValueNewIndexes = `${fixedOrNotValue}`;

        markdownContent = markdownContent.replaceAll(`!DESCRIPTION-ID-FOR-FINDINGS-SUMMARY-${i}!`, descriptionIdForFindingsSummaryIndexValueNewIndexes);
        markdownContent = markdownContent.replaceAll(`!DESCRIPTION-TITLE-FOR-FINDINGS-SUMMARY-${i}!`, descriptionTitileForFindingsSummaryIndexValueNewIndexes);
        markdownContent = markdownContent.replace(`!DESCRIPTION-SEVERITY-FOR-FINDINGS-SUMMARY-${i}!`, descriptionSeverityForFindingsSummaryIndexValueNewIndexes);
        markdownContent = markdownContent.replace(`!INFORMATION-FOR-IMPACT-SEVERITY-${i}!`, informationForImpactSeverityIndexValueNewIndexes);
        markdownContent = markdownContent.replace(`!INFORMATION-FOR-LIKELIHOOD-SEVERITY-${i}!`, informationForLikelihoodSeverityValueNewIndexes);
        markdownContent = markdownContent.replace(`!TEXT-FOR-VULNERABILITY-DETAILS-${i}!`, textForVulnerabilityDetailsValueNewIndexes);
        markdownContent = markdownContent.replace(`!TEXT-FOR-RECOMMENDATION-${i}!`, textForRecommendationValueNewIndexes);
        markdownContent = markdownContent.replace(`!CODE-TYPE-${i}!`, codeTypeValueIndexes);
        markdownContent = markdownContent.replace(`!CODE-SNIPET-${i}!`, codeSnippetValueIndexes);
        markdownContent = markdownContent.replace(`!FIXED-OR-NOT-${i}!`, fixedOrNotValueNewIndexes);
    }

    return markdownContent;
}

async function populateFiles(markdownContent) {
    for (let i = 0; i < fileCounter; i++) {
        const fileId = `FILE-FOR-THE-SCOPE-${i}`;
        const fileValue = document.getElementById(fileId).value;
        const fileByNewIndexes = `${fileValue}`;
        markdownContent = markdownContent.replace(`!FILE-FOR-THE-SCOPE-${i}!`, fileByNewIndexes);
    }

    return markdownContent;
}

async function populateAuthors(markdownContent) {
    for (let i = 0; i < authorCounter; i++) {
        const authorId = `TEXT-FOR-AUDITED-BY-${i}`;
        const authorValue = document.getElementById(authorId).value;
        const auditedByNewIndexes = `${authorValue}`;
        markdownContent = markdownContent.replace(`!TEXT-FOR-AUDITED-BY-${i}!`, auditedByNewIndexes);
    }

    return markdownContent;
}

async function populateRolesAndActors(markdownContent) {
    for (let i = 0; i < roleAndActorCounter; i++) {
        const roleAndActorId = `TEXT-FOR-ROLES-AND-ACTORS-${i}`;
        const roleAndActorDescriptionId = `DESCRIPTION-FOR-ROLES-AND-ACTORS-${i}`;

        const roleAndActorValue = document.getElementById(roleAndActorId).value;
        const roleAndActorDescriptionValue = document.getElementById(roleAndActorDescriptionId).value;

        const roleAndActorIndexes = `${roleAndActorValue}`;
        const roleAndActorDescriptionIndexes = `${roleAndActorDescriptionValue}`;

        markdownContent = markdownContent.replace(`!TEXT-FOR-ROLES-AND-ACTORS-${i}!`, roleAndActorIndexes);
        markdownContent = markdownContent.replace(`!DESCRIPTION-FOR-ROLES-AND-ACTORS-${i}!`, roleAndActorDescriptionIndexes);
    }

    return markdownContent;
}

function addNewRolesAndActors() {
    const container = document.getElementById('roles-and-actors');
    const newRoleAndActorsId = `TEXT-FOR-ROLES-AND-ACTORS-${roleAndActorCounter}`;
    const newRoleAndActorsDescriptionId = `DESCRIPTION-FOR-ROLES-AND-ACTORS-${roleAndActorCounter}`;

    const newRoleAndActorsInput = document.createElement('textarea');
    newRoleAndActorsInput.setAttribute('id', newRoleAndActorsId);
    newRoleAndActorsInput.setAttribute('placeholder', newRoleAndActorsId);

    const newRoleAndActorsDescriptionInput = document.createElement('textarea');
    newRoleAndActorsDescriptionInput.setAttribute('id', newRoleAndActorsDescriptionId);
    newRoleAndActorsDescriptionInput.setAttribute('placeholder', newRoleAndActorsDescriptionId);

    const protocolText = document.getElementById('markdown-input');

    // Find the position where the existing section ends
    const existingSectionEnd = protocolText.value.indexOf('\n\n# Protocol Summary');

    // Insert the new entry after the existing section
    protocolText.value = protocolText.value.slice(0, existingSectionEnd) +
        `\n- ***!${newRoleAndActorsId}!*** - !${newRoleAndActorsDescriptionId}!` +
        protocolText.value.slice(existingSectionEnd);

    const newRoleAndActorsContainer = document.createElement('h1');
    const newRoleAndActorsDescriptionContainer = document.createElement('h1');
    newRoleAndActorsContainer.innerHTML = `Privileged Roles and Actors (Who?): <textarea placeholder="${newRoleAndActorsId}" id="${newRoleAndActorsId}"></textarea>`;
    newRoleAndActorsDescriptionContainer.innerHTML = `Privileged Roles and Actors (Description?): <textarea placeholder="${newRoleAndActorsDescriptionId}" id="${newRoleAndActorsDescriptionId}"></textarea>`;

    container.appendChild(newRoleAndActorsContainer);
    container.appendChild(newRoleAndActorsDescriptionContainer);

    roleAndActorCounter++;
}

function addNewAuthor() {
    const container = document.getElementById('authors-container'); // Change to the actual container ID
    const newAuthorId = `TEXT-FOR-AUDITED-BY-${authorCounter}`;

    const newAuthorInput = document.createElement('textarea');
    newAuthorInput.setAttribute('id', newAuthorId);
    newAuthorInput.setAttribute('placeholder', newAuthorId);

    const protocolText = document.getElementById('markdown-input');
    let searchText = `!TEXT-FOR-AUDITED-BY-${authorCounter - 1}!`;

    if (authorCounter === 0) {
        searchText = '!TEXT-FOR-AUDITED-BY!';
    }

    console.log(searchText);
    const cursorPos = protocolText.value.indexOf(searchText) + searchText.length;

    // If the search text is found
    if (cursorPos >= searchText.length) {
        const start = protocolText.value.substring(0, cursorPos);
        const end = protocolText.value.substring(cursorPos);

        protocolText.value = start + `\n- !${newAuthorId}!` + end;
    } else {
        // If the search text is not found, append the new author line at the end
        protocolText.value += `\n- !${newAuthorId}!`;
    }

    const newAuthorContainer = document.createElement('h1');
    newAuthorContainer.innerHTML = `The protocol was audited by: <textarea placeholder="${newAuthorId}" id="${newAuthorId}"></textarea>`;

    container.appendChild(newAuthorContainer);

    authorCounter++;
}


function addNewFile() {
    const container = document.getElementById('scope-for-this-security-review'); // Change to the actual container ID
    const newFileId = `FILE-FOR-THE-SCOPE-${fileCounter}`;

    const newFileInput = document.createElement('textarea');
    newFileInput.setAttribute('id', newFileId);
    newFileInput.setAttribute('placeholder', newFileId);

    const protocolText = document.getElementById('markdown-input');
    let searchText = `!FILE-FOR-THE-SCOPE-${fileCounter - 1}!`;

    if (fileCounter === 0) {
        searchText = '!FILE-FOR-THE-SCOPE!';
    }

    console.log(searchText);
    const cursorPos = protocolText.value.indexOf(searchText) + searchText.length;

    // If the search text is found
    if (cursorPos >= searchText.length) {
        const start = protocolText.value.substring(0, cursorPos);
        const end = protocolText.value.substring(cursorPos);

        protocolText.value = start + `\n* !${newFileId}!` + end;
    } else {
        // If the search text is not found, append the new author line at the end
        protocolText.value += `\n* !${newFileId}!`;
    }

    const newAuthorContainer = document.createElement('h1');
    newAuthorContainer.innerHTML = `The scope for this security review was the following: <textarea placeholder="${newFileId}" id="${newFileId}"></textarea>`;

    container.appendChild(newAuthorContainer);

    fileCounter++;
}

function addNewFindingsSummary() {
    const container = document.getElementById('findings-container');
    const newFindingsId = `DESCRIPTION-ID-FOR-FINDINGS-SUMMARY-${findingsCounter}`;
    const newFindingsTitle = `DESCRIPTION-TITLE-FOR-FINDINGS-SUMMARY-${findingsCounter}`;
    const newFindingsSeverity = `DESCRIPTION-SEVERITY-FOR-FINDINGS-SUMMARY-${findingsCounter}`;

    const protocolText = document.getElementById('markdown-input');
    let searchText = `| [!DESCRIPTION-ID-FOR-FINDINGS-SUMMARY-${findingsCounter - 1}!] | !DESCRIPTION-TITLE-FOR-FINDINGS-SUMMARY-${findingsCounter - 1}! | !DESCRIPTION-SEVERITY-FOR-FINDINGS-SUMMARY-${findingsCounter - 1}! |`;

    if (findingsCounter === 0) {
        searchText = '| [!DESCRIPTION-ID-FOR-FINDINGS-SUMMARY!] | !DESCRIPTION-TITLE-FOR-FINDINGS-SUMMARY! | !DESCRIPTION-SEVERITY-FOR-FINDINGS-SUMMARY! |';
    }

    const cursorPos = protocolText.value.indexOf(searchText) + searchText.length;

    // If the search text is found
    if (cursorPos >= searchText.length) {
        const start = protocolText.value.substring(0, cursorPos);
        const end = protocolText.value.substring(cursorPos);

        protocolText.value = start + `\n| [!${newFindingsId}!] | !${newFindingsTitle}! | !${newFindingsSeverity}! |` + end;
    } else {
        // If the search text is not found, append the new findings summary at the end
        protocolText.value += `\n| [!${newFindingsId}!] | !${newFindingsTitle}! | !${newFindingsSeverity}! |`;
    }

    protocolText.value += `\n
# [!DESCRIPTION-ID-FOR-FINDINGS-SUMMARY-${findingsCounter}!] !DESCRIPTION-TITLE-FOR-FINDINGS-SUMMARY-${findingsCounter}! !FIXED-OR-NOT-${findingsCounter}!

#### Severity

- ***Impact*** !INFORMATION-FOR-IMPACT-SEVERITY-${findingsCounter}!
- ***Likelihood*** !INFORMATION-FOR-LIKELIHOOD-SEVERITY-${findingsCounter}!

#### Vulnerability Details

!TEXT-FOR-VULNERABILITY-DETAILS-${findingsCounter}!

#### Recommendation

!TEXT-FOR-RECOMMENDATION-${findingsCounter}!

\`\`\`!CODE-TYPE-${findingsCounter}!
    !CODE-SNIPET-${findingsCounter}!
\`\`\`
`;


    // TABLE
    const newFindingsContainerID = document.createElement('h1');
    const newFindingsContainerTitle = document.createElement('h1');
    const newFindingsContainerSeverity = document.createElement('h1');
    newFindingsContainerID.innerHTML = `ID For Found Issue Summary: <textarea placeholder="${newFindingsId}" id="${newFindingsId}"></textarea>`
    newFindingsContainerTitle.innerHTML = `Title For Found Issue Summary: <textarea placeholder="${newFindingsTitle}" id="${newFindingsTitle}"></textarea>`
    newFindingsContainerSeverity.innerHTML = `Severity For Found Issue Summary: <textarea placeholder="${newFindingsSeverity}" id="${newFindingsSeverity}"></textarea>`

    container.appendChild(newFindingsContainerID);
    container.appendChild(newFindingsContainerTitle);
    container.appendChild(newFindingsContainerSeverity);

    // DETAILED
    const informationForImpactSeverity = document.createElement('h1');
    const informationForLikelihoodSeverity = document.createElement('h1');
    const textForVulnerabilityDetails = document.createElement('h1');
    const textForRecommendation = document.createElement('h1');
    const codeSnippet = document.createElement('h1');
    const codeType = document.createElement('h1');
    const fixedOrNot = document.createElement('h1');

    informationForImpactSeverity.innerHTML = `Information for impact severity: <textarea placeholder="INFORMATION-FOR-IMPACT-SEVERITY-${findingsCounter}" id="INFORMATION-FOR-IMPACT-SEVERITY-${findingsCounter}"></textarea>`
    informationForLikelihoodSeverity.innerHTML = `Information for likelihood severity: <textarea placeholder="INFORMATION-FOR-LIKELIHOOD-SEVERITY-${findingsCounter}" id="INFORMATION-FOR-LIKELIHOOD-SEVERITY-${findingsCounter}"></textarea>`
    textForVulnerabilityDetails.innerHTML = `Text for vulnerability details: <textarea placeholder="TEXT-FOR-VULNERABILITY-DETAILS-${findingsCounter}" id="TEXT-FOR-VULNERABILITY-DETAILS-${findingsCounter}"></textarea>`
    textForRecommendation.innerHTML = `Text for recommendation: <textarea placeholder="TEXT-FOR-RECOMMENDATION-${findingsCounter}" id="TEXT-FOR-RECOMMENDATION-${findingsCounter}"></textarea>`
    codeType.innerHTML = `Code type: <textarea placeholder="CODE-TYPE-${findingsCounter}" id="CODE-TYPE-${findingsCounter}"></textarea>`
    codeSnippet.innerHTML = `Code snippet: <textarea placeholder="CODE-SNIPET-${findingsCounter}" id="CODE-SNIPET-${findingsCounter}"></textarea>`
    fixedOrNot.innerHTML = `Fixed or not- copy and paste one of those ✅ or 📚:<textarea placeholder="FIXED-OR-NOT-${findingsCounter}" id="FIXED-OR-NOT-${findingsCounter}"></textarea>`

    container.appendChild(informationForImpactSeverity);
    container.appendChild(informationForLikelihoodSeverity);
    container.appendChild(textForVulnerabilityDetails);
    container.appendChild(textForRecommendation);
    container.appendChild(codeType);
    container.appendChild(codeSnippet);
    container.appendChild(fixedOrNot);


    findingsCounter++;
}
