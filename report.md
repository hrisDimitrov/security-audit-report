# Table of Contents

1. [Introduction](#introduction)
2. [Disclaimer](#disclaimer)
3. [About Me](#about-me)
4. [Privileged Roles and Actors](#privileged-roles-and-actors)
5. [Protocol Summary](#protocol-summary)
6. [Severity classification](#severity-classification)
7. [Security Assessment Summary](#security-assessment-summary)
8. [Findings Summary](#findings-summary)
9. [Detailed Summary](#detailed-summary)

# Introduction
!TEXT-FOR-INTRODUCTION!

The protocol was audited by:
- !TEXT-FOR-AUDITED-BY!

# Disclaimer
!TEXT-FOR-DISCLAIMER!

# About Me
!TEXT-FOR-ABOUT-ME!

# Privileged Roles and Actors
- ***!TEXT-FOR-ROLES-AND-ACTORS!*** - !DESCRIPTION-FOR-ROLES-AND-ACTORS!

# Protocol Summary
| Label | Description|
|:---|:---|
| Project Name | !PROJECT-NAME! |
| Repository | !REPOSITORY! |
| Audit Timeline | !AIDIT-TIMELINE! |
| Review Commit Hash | !REVIEW-COMMIT-HASH! |

# Severity classification
| Severity | Impact: High | Impact: Medium | Impact: Low |
|---|--- |---|---|
|Likelihood: High  |Critical |High  |Medium|
|Likelihood: Medium|High     |Medium|Low|
|Likelihood: Low   |Medium   |Low   |Low|

- ***Impact*** - the technical, economic and reputation damage of a successful attack
- ***Likelihood*** - the chance that a particular vulnerability gets discovered and exploited
- ***Severity*** - the overall criticality of the risk

# Security Assessment Summary
### Scope
The scope for this security review was the following:

* !FILE-FOR-THE-SCOPE!

The following number of issues were found, categorized by their severity:

* Critical : !NUMBER-OF-CRITICAL-ISSUES! issues
* High : !NUMBER-OF-HIGH-ISSUES! issues
* Medium : !NUMBER-OF-MEDIUM-ISSUES! issue
* Low : !NUMBER-OF-LOW-ISSUES! issues
* Informational : !NUMBER-OF-INFORMATIONAL-ISSUES! issues

# Findings Summary
| ID | Title | Severity |
|---|---|---|
| [!DESCRIPTION-ID-FOR-FINDINGS-SUMMARY!] | !DESCRIPTION-TITLE-FOR-FINDINGS-SUMMARY! | !DESCRIPTION-SEVERITY-FOR-FINDINGS-SUMMARY! |

# Detailed Summary
# [!DESCRIPTION-ID-FOR-FINDINGS-SUMMARY!] !DESCRIPTION-TITLE-FOR-FINDINGS-SUMMARY! !FIXED-OR-NOT!

#### Severity

- ***Impact*** !INFORMATION-FOR-IMPACT-SEVERITY!
- ***Likelihood*** !INFORMATION-FOR-LIKELIHOOD-SEVERITY!

#### Vulnerability Details

!TEXT-FOR-VULNERABILITY-DETAILS!

#### Recommendation

!TEXT-FOR-RECOMMENDATION!

```!CODE-TYPE!
	!CODE-SNIPET!
```
